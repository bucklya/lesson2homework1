package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner numberInputOne = new Scanner(System.in);
        Scanner numberInputTwo = new Scanner(System.in);
        Scanner numberInputThree = new Scanner(System.in);
        System.out.println("Введите первое число: ");
        int numberOne = numberInputOne.nextInt();
        System.out.println("Введите второе число: ");
        int numberTwo = numberInputTwo.nextInt();
        System.out.println("Введите третье число: ");
        int numberThree = numberInputThree.nextInt();

        //System.out.println(numberOne + " " + numberTwo + " " + numberThree);

        if (numberOne > numberTwo && numberOne > numberThree) {
            if (numberTwo > numberThree) {
                System.out.println("Сумма квадратов двух самых больших чисел равна " + (numberOne * numberOne + numberTwo * numberTwo));
            } else {
                System.out.println("Сумма квадратов двух самых больших чисел равна " + (numberOne * numberOne + numberThree * numberThree));
            }
        } else {
            System.out.println("Сумма квадратов двух самых больших чисел равна " + (numberTwo * numberTwo + numberThree * numberThree));
        }
    }
}